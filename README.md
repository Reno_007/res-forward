# ResForward

#### 介绍
nodejs实现资源转发的功能

#### 安装教程
npm install
node index.js

#### 使用说明
浏览器上访问地址: http://localhost:11100/resForward?url=aHR0cDovL3A5Lmh1b3NoYW5pbWcuY29tL2ltZy9tb3NhaWMtbGVnYWN5LzcxYTYwMDBkYTQ3MWUzYzA0ODRjfjEwMHgxMDAuanBn

url: 是我们实际请求的地址的base64值

