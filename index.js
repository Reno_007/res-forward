const express = require("express");
const url = require("url");
const http = require("http");
const https = require('https');

var app = express();
// 设置我们的跨域访问
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    next();
});
app.on("error", (err)=>{
    console.log("server error:", err);
})
// end
let nHttpListenPort = 11100;
app.listen(nHttpListenPort, (err)=>{
    if (err) {
        console.log("http listening error on port=", nHttpListenPort);
    }
    else{
        console.log("http listening on port=", nHttpListenPort);
    }
});

function base64_decode(base64_str){
    var buf = Buffer.from(base64_str, "base64");
    let result = buf.toString("utf8");
    return result;
}

app.get("/resForward", function(request, respones) {
    var myObj = url.parse(request.url,true);
    let requestUrl = myObj.query.url;
    requestUrl = base64_decode(requestUrl);
    console.log("resForward requestUrl:"+requestUrl);

    let parsedUrl = url.parse(requestUrl);
    let httpTemp = (parsedUrl.protocol == "https:") ? https : http;

    let req = httpTemp.get(requestUrl, (res)=>{
        respones.writeHead(res.statusCode, res.headers);
        res.on('data', function (chunk) {
            respones.write(chunk);
        });
        res.on('end', function () {
            respones.end();
        });
    });
    req.on('error', function (e) {
        respones.writeHead(404);
        respones.end();
    });
});